DROP TABLE IF EXISTS users;

CREATE TABLE IF NOT EXISTS users (
    user_login TEXT PRIMARY KEY,
    user_password TEXT NOT NULL
);

DROP TABLE IF EXISTS groups;

CREATE TABLE IF NOT EXISTS groups (
    group_id INTEGER PRIMARY KEY,
    group_name TEXT NOT NULL,
    group_description TEXT NOT NULL
);

DROP TABLE IF EXISTS groups_users;

CREATE TABLE IF NOT EXISTS groups_users (
    user_login TEXT,
    group_id INTEGER,
    PRIMARY KEY (user_login, group_id),
    FOREIGN KEY (user_login) REFERENCES users (user_login)
                                        ON DELETE CASCADE
                                        ON UPDATE NO ACTION,
    FOREIGN KEY (group_id) REFERENCES groups (group_id)
                                        ON DELETE CASCADE
                                        ON UPDATE NO ACTION
);