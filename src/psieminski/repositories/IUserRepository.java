package psieminski.repositories;

import psieminski.UserDTO;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

	List<UserDTO> findByName(String username) throws SQLException;
}