package psieminski.repositories;

import java.security.InvalidKeyException;
import java.sql.*;

import psieminski.DTOBase;

public interface IRepository<TDTO extends DTOBase> {

	default Connection getConnection() throws SQLException {
		Connection connection;

		try {
			connection = DriverManager.getConnection("jdbc:sqlite:testDB.sqlite");
			connection.setAutoCommit(false);
			return connection;
		} catch (SQLException e) {
			throw new SQLException("Connection could not be established.");
		}
	}

//	default PreparedStatement executeStatement(String sqlCode) throws SQLException {
//		return getConnection().prepareStatement(sqlCode);
//	}

	void add(TDTO dto) throws SQLException;

	void update(TDTO dto, String s1, String s2) throws SQLException;

	void delete(TDTO dto) throws SQLException;

	TDTO findById(int id) throws SQLException, InvalidKeyException;

	void beginTransaction(Connection connection) throws SQLException;

	void commitTransaction(Connection connection) throws SQLException;

	void rollbackTransaction(Connection connection) throws SQLException;
	
	int getCount() throws SQLException;
	
	boolean exists(TDTO dto) throws SQLException;
}