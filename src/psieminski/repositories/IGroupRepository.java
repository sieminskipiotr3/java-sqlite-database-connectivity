package psieminski.repositories;

import java.sql.SQLException;
import java.util.List;

import psieminski.GroupDTO;

public interface IGroupRepository extends IRepository<GroupDTO> {

	List<GroupDTO> findByName(String name) throws SQLException;
}