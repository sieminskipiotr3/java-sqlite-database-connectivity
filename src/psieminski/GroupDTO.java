package psieminski;

import psieminski.repositories.IGroupRepository;

import java.security.InvalidKeyException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GroupDTO extends DTOBase implements IGroupRepository {

    private String _name;
    private String _description;
    private List<UserDTO> _users;

    public GroupDTO() {
    }

    public GroupDTO(int id, String name, String description) {
        super(id);
        _name = name;
        _description = description;
    }

    public String getName() {
        if (this._name == null) {
            throw new IllegalStateException("Field has no value, please set name first.");
        }
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getDescription() {
        if (this._description == null) {
            throw new IllegalStateException("Field has no value, please set description first.");
        }
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public List<UserDTO> getUsers() throws SQLException {
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "SELECT * FROM users WHERE user_login IN (SELECT user_login FROM groups_users WHERE group_id = ?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, this.getId());
        try {
            ResultSet rs = statement.executeQuery();
            List<UserDTO> users = new ArrayList<>();
            while (rs.next()) {
                UserDTO user = new UserDTO();
                user.setPassword(rs.getString("user_password"));
                user.setLogin(rs.getString("user_login"));
                users.add(user);
            }
            setUsers(users);
            commitTransaction(connection);
        } catch (SQLException e) {
            rollbackTransaction(connection);
            throw new SQLException("Transaction could not be finished.");
        }
        connection.close();
        return _users;
    }

    public void setUsers(List<UserDTO> users) throws SQLException {
        _users = users;

        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "DELETE FROM groups_users WHERE group_id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, getId());
        try {
            statement.executeUpdate();
            commitTransaction(connection);
        } catch (SQLException e) {
            rollbackTransaction(connection);
            throw new SQLException("Deletion not possible");
        }

        connection.close();

        for (UserDTO u : users) {
            Connection conn = getConnection();
            beginTransaction(conn);
            String insertQuery = "INSERT INTO groups_users (user_login, group_id) VALUES (?,?)";
            PreparedStatement insertStatement = conn.prepareStatement(insertQuery);
            insertStatement.setString(1, u.getLogin());
            insertStatement.setInt(2, getId());
            try {
                insertStatement.executeUpdate();
                commitTransaction(conn);
            } catch (SQLException e) {
                rollbackTransaction(conn);
                throw new SQLException("Insertion not possible");
            }
            conn.close();
        }

    }

    public void addUser(UserDTO user) throws SQLException {
        if (_users == null) {
            _users = new LinkedList<>();
        }
        _users.add(user);
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "INSERT INTO groups_users (user_login, group_id) VALUES (?,?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setInt(2, getId());
        try {
            statement.executeUpdate();
            commitTransaction(connection);
        } catch (SQLException e) {
            rollbackTransaction(connection);
            throw new SQLException("Row could not have been inserted. Check if does not already exist.");
        }
        connection.close();
    }

    public void deleteUser(UserDTO user) throws SQLException {
        if (_users != null) {
            _users.remove(user);
        }
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "DELETE FROM groups_users WHERE user_login = ? AND group_id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setInt(2, getId());
        try {
            statement.executeUpdate();
            commitTransaction(connection);
        } catch (SQLException e) {
            rollbackTransaction(connection);
            throw new SQLException("Row could not have been deleted");
        }
        connection.close();
    }

    public ResultSet readByQuery(String sqlQuery) throws SQLException {
        Connection connection = getConnection();
        beginTransaction(connection);
        PreparedStatement statement = connection.prepareStatement(sqlQuery);
        return statement.executeQuery();
    }

    @Override
    public List<GroupDTO> findByName(String name) throws SQLException {
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "SELECT group_id, group_name, group_description FROM groups WHERE group_name LIKE ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, "%" + name + "%");
        ResultSet rs = statement.executeQuery();
        List<GroupDTO> returnList = new ArrayList<>();
        while (rs.next()) {
            GroupDTO group = new GroupDTO();
            group.setDescription(rs.getString("group_description"));
            group.setName(rs.getString("group_name"));
            group.setId(rs.getInt("group_id"));
            returnList.add(group);
        }

        if (returnList.isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("There are no groups with matching name");
        }

        connection.close();
        return returnList;
    }

    @Override
    public void add(GroupDTO dto) throws SQLException {
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "INSERT INTO groups (group_id, group_name, group_description) VALUES (? , ?, ?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, dto.getId());
        statement.setString(2, dto.getName());
        statement.setString(3, dto.getDescription());
        try {
            statement.executeUpdate();
            commitTransaction(connection);
        } catch (SQLException e) {
            rollbackTransaction(connection);
            throw new SQLException("Statement failed to be executed. Check if group does not already exist");
        }
        connection.close();
    }

    @Override
    public void update(GroupDTO dto, String name, String description) throws SQLException {
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "UPDATE groups SET group_name = ?" +
                ", group_description = ? WHERE group_id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setInt(3, dto.getId());
        try {
            statement.executeUpdate();
            commitTransaction(connection);
        } catch (SQLException e) {
            rollbackTransaction(connection);
            throw new SQLException("Update not possible. Check if group chosen exists.");
        }
        connection.close();
    }

    @Override
    public void delete(GroupDTO dto) throws SQLException {
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "DELETE FROM groups WHERE group_id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, dto.getId());
        try {
            statement.executeUpdate();
            commitTransaction(connection);
        } catch (SQLException e) {
            rollbackTransaction(connection);
            throw new SQLException("Deletion not possible, check parameters.");
        }
        connection.close();
    }

    @Override
    public GroupDTO findById(int id) throws SQLException, InvalidKeyException {
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "SELECT * FROM groups WHERE group_id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        if (!rs.next()) {
            throw new InvalidKeyException("There exists no group with provided id");
        }
        GroupDTO group = new GroupDTO();
        group.setId(rs.getInt("group_id"));
        group.setName(rs.getString("group_name"));
        group.setDescription(rs.getString("group_description"));
        connection.close();
        return group;
    }

    @Override
    public void beginTransaction(Connection connection) throws SQLException {
        connection.beginRequest();
    }

    @Override
    public void commitTransaction(Connection connection) throws SQLException {
        connection.commit();
    }

    @Override
    public void rollbackTransaction(Connection connection) throws SQLException {
        connection.rollback();
        connection.close();
    }

    @Override
    public int getCount() throws SQLException {
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "SELECT COUNT(*) AS number_of_groups FROM groups";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet rs = statement.executeQuery();
        int result = rs.getInt("number_of_groups");
        connection.close();
        return result;
    }

    @Override
    public boolean exists(GroupDTO dto) throws SQLException {
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "SELECT * FROM groups WHERE group_id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, dto.getId());
        ResultSet rs = statement.executeQuery();
        boolean exists = rs.next();
        connection.close();
        return exists;
    }
}