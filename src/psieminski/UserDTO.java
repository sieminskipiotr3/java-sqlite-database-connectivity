package psieminski;

import psieminski.repositories.IUserRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class UserDTO extends DTOBase implements IUserRepository {

    private String _login;
    private String _password;
    private List<GroupDTO> _groups;

    public UserDTO() {
    }

    public UserDTO(int id, String login, String password) {
        super(id);
        _login = login;
        _password = password;
    }

    public String getLogin() {
        if (this._login == null) {
            throw new IllegalStateException("Field has no value, please set login first.");
        }
        return _login;
    }

    public void setLogin(String login) {
        _login = login;
    }

    public String getPassword() {
        if (this._password == null) {
            throw new IllegalStateException("Field has no value, please set password first.");
        }
        return _password;
    }

    public void setPassword(String password) {
        _password = password;
    }

    public List<GroupDTO> getGroups() throws SQLException {
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "SELECT * FROM groups WHERE group_id IN (SELECT group_id FROM groups_users WHERE user_login = ?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, this._login);
        try {
            ResultSet rs = statement.executeQuery();
            List<GroupDTO> groups = new ArrayList<>();
            while (rs.next()) {
                GroupDTO group = new GroupDTO();
                group.setId(rs.getInt("group_id"));
                group.setName(rs.getString("group_name"));
                group.setDescription(rs.getString("group_description"));
                groups.add(group);
            }
            setGroups(groups);
            commitTransaction(connection);
        } catch (SQLException e) {
            rollbackTransaction(connection);
            throw new SQLException("Transaction could not be finished.");
        }
        connection.close();
        return _groups;
    }

    public void setGroups(List<GroupDTO> groups) throws SQLException {
        _groups = groups;

        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "DELETE FROM groups_users WHERE user_login = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, this._login);
        try {
            statement.executeUpdate();
            commitTransaction(connection);
        } catch (SQLException e) {
            rollbackTransaction(connection);
            throw new SQLException("Deletion not possible");
        }

        connection.close();

        for (GroupDTO g : _groups) {
            Connection conn = getConnection();
            beginTransaction(conn);
            String insertQuery = "INSERT INTO groups_users (user_login, group_id) VALUES (?,?)";
            PreparedStatement insertStatement = conn.prepareStatement(insertQuery);
            insertStatement.setString(1, this._login);
            insertStatement.setInt(2, g.getId());
            try {
                insertStatement.executeUpdate();
                commitTransaction(conn);
            } catch (SQLException e) {
                rollbackTransaction(conn);
                throw new SQLException("Insertion not possible");
            }
            conn.close();
        }
    }

    public void addGroup(GroupDTO group) throws SQLException {
        if (_groups == null) {
            _groups = new LinkedList<>();
        }
        _groups.add(group);
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "INSERT INTO groups_users (user_login, group_id) VALUES (?,?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, this._login);
        statement.setInt(2, group.getId());
        try {
            statement.executeUpdate();
            commitTransaction(connection);
        } catch (SQLException e) {
            rollbackTransaction(connection);
            throw new SQLException("Row could not have been inserted. Check if does not already exist.");
        }
        connection.close();
    }

    public void deleteGroup(GroupDTO group) throws SQLException {
        if (_groups != null) {
            _groups.remove(group);
        }
        Connection connection = getConnection();
        beginTransaction(connection);
        String query = "DELETE FROM groups_users WHERE user_login = ? AND group_id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, this._login);
        statement.setInt(2, group.getId());
        try {
            statement.executeUpdate();
            commitTransaction(connection);
        } catch (SQLException e) {
            rollbackTransaction(connection);
            throw new SQLException("Row could not have been deleted");
        }
        connection.close();
    }

    public ResultSet readByQuery(String sqlQuery) throws SQLException {
        Connection connection = getConnection();
        beginTransaction(connection);
        PreparedStatement statement = connection.prepareStatement(sqlQuery);
        return statement.executeQuery();
    }

    @Override
    public void add(UserDTO dto) throws SQLException {
        Connection conn = getConnection();
        beginTransaction(conn);
        String query = "INSERT INTO users (user_login, user_password) VALUES (? , ?)";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, dto.getLogin());
        statement.setString(2, dto.getPassword());
        try {
            statement.executeUpdate();
            commitTransaction(conn);
        } catch (SQLException e) {
            rollbackTransaction(conn);
            throw new SQLException("Statement failed to be executed. Check if user does not already exist in DB.");
        }
        conn.close();
    }

    @Override
    public void update(UserDTO dto, String password, String login) throws SQLException {
        Connection conn = getConnection();
        beginTransaction(conn);
        String query = "UPDATE users SET user_login = ?, user_password = ? WHERE user_login = ? AND user_password = ?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, login);
        statement.setString(2, password);
        statement.setString(3, dto.getLogin());
        statement.setString(4, dto.getPassword());
        try {
            statement.executeUpdate();
            commitTransaction(conn);
        } catch (SQLException e) {
            rollbackTransaction(conn);
            throw new SQLException("Field could not be updated. Check values type");
        }
        conn.close();
    }

    @Override
    public void delete(UserDTO dto) throws SQLException {
        Connection conn = getConnection();
        beginTransaction(conn);
        String query = "DELETE FROM users WHERE user_login = ? AND user_password = ?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, dto.getLogin());
        statement.setString(2, dto.getPassword());
        try {
            statement.executeUpdate();
            commitTransaction(conn);
        } catch (SQLException e) {
            rollbackTransaction(conn);
            throw new SQLException("Could not delete the row.");
        }
        conn.close();
    }

    @Override
    public UserDTO findById(int id) {
        System.out.println("There has not been implemented user_id in this version");
        return null;
    }

    @Override
    public void beginTransaction(Connection connection) throws SQLException {
        connection.beginRequest();
    }

    @Override
    public void commitTransaction(Connection connection) throws SQLException {
        connection.commit();
    }

    @Override
    public void rollbackTransaction(Connection connection) throws SQLException {
        connection.rollback();
        connection.close();
    }

    @Override
    public int getCount() throws SQLException {
        Connection conn = getConnection();
        beginTransaction(conn);
        String query = "SELECT COUNT(*) AS number_of_users FROM users";
        PreparedStatement statement = conn.prepareStatement(query);
        ResultSet rs = statement.executeQuery();
        int result = rs.getInt("number_of_users");
        conn.close();
        return result;
    }

    @Override
    public boolean exists(UserDTO dto) throws SQLException {
        Connection conn = getConnection();
        beginTransaction(conn);
        String query = "SELECT * FROM users WHERE user_login = ? AND user_password = ?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, dto.getLogin());
        statement.setString(2, dto.getPassword());
        ResultSet rs = statement.executeQuery();
        boolean result = rs.next();
        conn.close();
        return result;
    }

    @Override
    public List<UserDTO> findByName(String username) throws SQLException {
        Connection conn = getConnection();
        beginTransaction(conn);
        String query = "SELECT * FROM users WHERE user_login LIKE ?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, "%" + username + "%");
        ResultSet rs = statement.executeQuery();
        List<UserDTO> returnList = new ArrayList<>();
        while (rs.next()) {
            UserDTO user = new UserDTO();
            user.setLogin(rs.getString("user_login"));
            user.setPassword(rs.getString("user_password"));
            returnList.add(user);
        }

        if (returnList.isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("There are no users with matching user_login");
        }

        conn.close();
        return returnList;
    }
}