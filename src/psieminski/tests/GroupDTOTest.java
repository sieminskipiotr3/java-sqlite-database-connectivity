package psieminski.tests;

import org.junit.jupiter.api.Test;
import psieminski.GroupDTO;

import java.security.InvalidKeyException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class GroupDTOTest {

    /* Functions in this class are written the same way they are
     * in GroupDTO. The reason they're re-written is because we
     * want to roll back and not commit none of the changes made by queries */

    @Test
    void findByName() throws SQLException {
        GroupDTO group = new GroupDTO();
        group.setId(2);
        group.setDescription("Group to be found by name");
        group.setName("NAME");
        group.add(group);
        assertEquals(group.getName(), group.findByName("AM").get(0).getName());
        group.delete(group);
    }

    @Test
    void add() throws SQLException {
        GroupDTO group = new GroupDTO();
        group.setId(1);
        group.setDescription("Very funny group");
        group.setName("FUNNY");
        Connection connection = group.getConnection();
        group.beginTransaction(connection);
        String query = "INSERT INTO groups (group_id, group_name, group_description) VALUES (? , ?, ?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, group.getId());
        statement.setString(2, group.getName());
        statement.setString(3, group.getDescription());
        statement.executeUpdate();
        String assertQuery = "SELECT group_name FROM groups WHERE group_id = ?";
        PreparedStatement assertStatement = connection.prepareStatement(assertQuery);
        assertStatement.setInt(1, group.getId());
        ResultSet rs = assertStatement.executeQuery();
        assertEquals(group.getName(), rs.getString("group_name"));
        connection.rollback();
        connection.close();
    }

    @Test
    void update() throws SQLException {
        GroupDTO group = new GroupDTO();
        Connection connection = group.getConnection();
        group.beginTransaction(connection);
        String query = "UPDATE groups SET group_name = ?" +
                ", group_description = ? WHERE group_id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, "NewName");
        statement.setString(2, "NewDescription");
        statement.setInt(3, 10000);
        statement.executeUpdate();
        String assertQuery = "SELECT * FROM groups WHERE group_name = ?";
        PreparedStatement assertStatement = connection.prepareStatement(assertQuery);
        assertStatement.setString(1, "Random1");
        ResultSet rs = assertStatement.executeQuery();
        assertFalse(rs.next());
        connection.rollback();
        connection.close();
    }

    @Test
    void delete() throws SQLException {
        GroupDTO group = new GroupDTO();
        Connection connection = group.getConnection();
        group.beginTransaction(connection);
        String query = "DELETE FROM groups WHERE group_id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, 10000);
        statement.executeUpdate();
        String assertQuery = "SELECT * FROM groups WHERE group_id = ?";
        PreparedStatement assertStatement = connection.prepareStatement(assertQuery);
        assertStatement.setInt(1, 10000);
        ResultSet rs = assertStatement.executeQuery();
        assertFalse(rs.next());
        connection.rollback();
        connection.close();
    }

    @Test
    void findById() throws SQLException, InvalidKeyException {
        GroupDTO group = new GroupDTO();
        group.setId(4);
        group.setDescription("Group to be found by id");
        group.setName("ID");
        group.add(group);
        assertEquals(group.getId(), group.findById(4).getId());
        group.delete(group);
    }

    @Test
    void getCount() throws SQLException {
        GroupDTO group = new GroupDTO();
        assertEquals(5, group.getCount());
    }

    @Test
    void exists() throws SQLException {
        GroupDTO group = new GroupDTO();
        group.setId(3);
        group.setDescription("By William Shakespear");
        group.setName("ExistsOrNotExists");
        group.add(group);
        assertTrue(group.exists(group));
        group.delete(group);
    }

    @Test
    void readByQuery() throws SQLException {
        GroupDTO group = new GroupDTO();
        int result = group.readByQuery("SELECT COUNT(*) AS all_groups FROM groups").getInt("all_groups");
        assertEquals(5, result);
    }

}