package psieminski.tests;

import org.junit.jupiter.api.Test;
import psieminski.GroupDTO;
import psieminski.UserDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserDTOTest {

    /* Functions in this class are written the same way they are
     * in UserDTO. The reason they're re-written is because we
     * want to roll back and not commit none of the changes made by queries */

    @org.junit.jupiter.api.Test
    void add() throws SQLException {
        UserDTO user = new UserDTO();
        user.setLogin("try");
        user.setPassword("adding");
        Connection conn = user.getConnection();
        String query = "INSERT INTO users (user_login, user_password) VALUES (? , ?)";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPassword());
        statement.executeUpdate();
        //assert
        String assertQuery = "SELECT user_login FROM users WHERE user_login = ?";
        PreparedStatement assertStatement = conn.prepareStatement(assertQuery);
        assertStatement.setString(1, user.getLogin());
        ResultSet rs = assertStatement.executeQuery();
        assertEquals(user.getLogin(), rs.getString("user_login"));
        //rollback
        conn.rollback();
        conn.close();
    }

    @org.junit.jupiter.api.Test
    void update() throws SQLException {
        UserDTO user = new UserDTO();
        user.setLogin("update");
        user.setPassword("check");
        user.add(user);
        Connection conn = user.getConnection();
        user.beginTransaction(conn);
        String query = "UPDATE users SET user_login = ?, user_password = ? WHERE user_login = ? AND user_password = ?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, "newLogin");
        statement.setString(2, "newPassword");
        statement.setString(3, user.getLogin());
        statement.setString(4, user.getPassword());
        try {
            statement.executeUpdate();
            conn.commit();
            String assertQuery = "SELECT user_login FROM users WHERE user_login = ?";
            PreparedStatement assertStatement = conn.prepareStatement(assertQuery);
            assertStatement.setString(1, "newLogin");
            ResultSet rs = assertStatement.executeQuery();
            assertEquals("newLogin", rs.getString("user_login"));
        } catch (SQLException e) {
            user.rollbackTransaction(conn);
            throw new SQLException("Field could not be updated. Check values type");
        }
        String deleteQuery = "DELETE FROM users WHERE user_login = 'newLogin'";
        PreparedStatement deleteStatement = conn.prepareStatement(deleteQuery);
        deleteStatement.executeUpdate();
        conn.commit();
        conn.close();
    }

    @org.junit.jupiter.api.Test
    void delete() throws SQLException {
        UserDTO user = new UserDTO();
        user.setLogin("delete");
        user.setPassword("assessment");
        user.add(user);
        Connection conn = user.getConnection();
        user.beginTransaction(conn);
        String query = "DELETE FROM users WHERE user_login = ? AND user_password = ?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPassword());
        try {
            statement.executeUpdate();
            user.commitTransaction(conn);
        } catch (SQLException e) {
            user.rollbackTransaction(conn);
            throw new SQLException("Could not delete the row.");
        }

        String assertQuery = "SELECT user_login FROM users WHERE user_login = ?";
        PreparedStatement assertStatement = conn.prepareStatement(assertQuery);
        assertStatement.setString(1, user.getLogin());
        ResultSet rs = assertStatement.executeQuery();
        assertFalse(rs.next());

        conn.close();
    }

    @org.junit.jupiter.api.Test
    void findById() {
    }

    @org.junit.jupiter.api.Test
    void getCount() throws SQLException {
        UserDTO user = new UserDTO();
        assertEquals(5, user.getCount());
    }

    @org.junit.jupiter.api.Test
    void exists() throws SQLException {
        UserDTO user = new UserDTO();
        user.setLogin("exists");
        user.setPassword("orNotExists");
        user.add(user);
        assertTrue(user.exists(user));
        user.delete(user);
    }

    @org.junit.jupiter.api.Test
    void findByName() throws SQLException {
        UserDTO user = new UserDTO();
        user.setLogin("balubalubalu");
        user.setPassword("byName");
        user.add(user);
        //checking for wildcard match
        assertEquals(user.getPassword(), user.findByName("balu").get(0).getPassword());
        user.delete(user);
    }

    @Test
    void readByQuery() throws SQLException {
        UserDTO user = new UserDTO();
        int result = user.readByQuery("SELECT COUNT(*) AS all_users FROM users").getInt("all_users");
        assertEquals(5, result);
    }

    @Test
    void setGroups() throws SQLException {

        GroupDTO group1 = new GroupDTO(1, "name1", "description1");
        GroupDTO group2 = new GroupDTO(2, "name2", "description2");
        GroupDTO group3 = new GroupDTO(3, "name3", "description3");
        List<GroupDTO> groups = new ArrayList<>();
        groups.add(group1);
        groups.add(group2);
        groups.add(group3);
        UserDTO user = new UserDTO();
        user.setLogin("loginMY1");
        user.setPassword("passwordMY1");

        Connection conn = user.getConnection();

        for (GroupDTO g : groups) {
            user.beginTransaction(conn);
            String insertQuery = "INSERT INTO groups_users (user_login, group_id) VALUES (?,?)";
            PreparedStatement insertStatement = conn.prepareStatement(insertQuery);
            insertStatement.setString(1, user.getLogin());
            insertStatement.setInt(2, g.getId());
            insertStatement.executeUpdate();
        }

        String assertQuery = "SELECT * FROM groups_users WHERE user_login = ?";
        PreparedStatement assertStatement = conn.prepareStatement(assertQuery);
        assertStatement.setString(1, user.getLogin());
        ResultSet rs = assertStatement.executeQuery();
        assertEquals(1, rs.getInt("group_id"));

        conn.rollback();
    }

}